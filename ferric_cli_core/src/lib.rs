// lib: ferric_cli_core
// path: src/lib.rs

#[derive(Debug)]
pub enum CliError {
    MissingArgument(String),
    InvalidArgumentValue(String),
    HelpRequested,
    // ... other errors ...
}

pub fn split_arg(arg: &str) -> (&str, Option<&str>) {
    let mut parts = arg.splitn(2, '=');
    (parts.next().unwrap(), parts.next())
}

pub struct CliArg {
    pub name: &'static str,
    pub description: &'static str,
}

pub struct ParsedArg {
    pub name: String,
    pub value: Option<String>,
}

pub struct ParserConfig {
    // ... any settings like default prefixes, etc. ...
}

impl ParserConfig {
    pub fn new() -> Self {
        Self {
            // ... initialize with default settings ...
        }
    }
}