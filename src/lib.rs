//path: src/lib.rs
//lib: ferric_cli_tool

// Re-export everything from ferric-cli-core
pub use ferric_cli_core::*;

// Re-export the macro from ferric-cli-macro
pub use ferric_cli_macro::CliParser;