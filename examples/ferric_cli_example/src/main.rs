//path: src/main.rs
// bin: ferric_cli_example

use ferric_cli_tool::{CliParser, CliError};

#[derive(Debug, CliParser, Default)]
struct Args {
    #[cli(short="c", long="config", desc="set the config", long_desc="even longer desc.......")]
    config: Option<String>,
    #[cli(short="v", long="verbose", desc="set the verbose", long_desc="even longer desc.......")]
    verbose: bool,
    #[cli(short="o", long="output", desc="set the output", long_desc="even longer desc.......")]
    output: Option<String>,
}

fn main() {
    match Args::parse_args() {
        Ok(args) => {
            // Use the parsed args here...
            println!("Parsed args: {:?}", args);
        }
        Err(CliError::HelpRequested) => {
            // Help message has already been printed by display_help
        }
        Err(e) => {
            eprintln!("Error: {:?}", e);
        }
    }
}
