// lib: ferric_cli_macro
// path: src/lib.rs

extern crate proc_macro;

use proc_macro::TokenStream;
use syn::{parse_macro_input, DeriveInput, Data, LitStr};

use quote::quote;

#[derive(Debug)]
struct CliAttribute {
    short: Option<String>,
    long: Option<String>,
    desc: Option<String>,
    long_desc: Option<String>,
}

#[proc_macro_derive(CliParser, attributes(cli))]
pub fn cli_parser_derive(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let struct_name = &input.ident;

    let fields = if let Data::Struct(data) = &input.data {
        &data.fields
    } else {
        panic!("CliParser can only be applied to structs!");
    };

    let cli_attrs: Vec<CliAttribute> = fields.iter().map(|field| {
        let mut attr_data = CliAttribute {
            short: None,
            long: None,
            desc: None,
            long_desc: None,
        };

        for attr in &field.attrs {
            if attr.path().is_ident("cli") {
                attr.parse_nested_meta(|meta| {
                    if meta.path.is_ident("short") {
                        let value_stream = meta.value()?;
                        let s: LitStr = value_stream.parse()?;
                        attr_data.short = Some(s.value());
                    } else if meta.path.is_ident("long") {
                        let value_stream = meta.value()?;
                        let s: LitStr = value_stream.parse()?;
                        attr_data.long = Some(s.value());
                    } else if meta.path.is_ident("desc") {
                        let value_stream = meta.value()?;
                        let s: LitStr = value_stream.parse()?;
                        attr_data.desc = Some(s.value());
                    } else if meta.path.is_ident("long_desc") {
                        let value_stream = meta.value()?;
                        let s: LitStr = value_stream.parse()?;
                        attr_data.long_desc = Some(s.value());
                    } else {
                        return Err(meta.error("unsupported cli property"));
                    }
                    Ok(())
                }).unwrap_or_default();
            }
        }

        attr_data
    }).collect();

    let arg_matching_tokens: Vec<_> = fields.iter().zip(cli_attrs.iter()).filter_map(|(field, attr)| {
        if attr.short.is_none() && attr.long.is_none() {
            return None;
        }

        let field_ident = &field.ident;

        // Check field type
        let assignment_logic = match &field.ty {
            syn::Type::Path(type_path) if type_path.path.is_ident("String") => {
                quote! {
            result.#field_ident = args_iter.next().unwrap_or_default().to_string();
        }
            },
            syn::Type::Path(type_path) if type_path.path.is_ident("bool") => {
                quote! {
            result.#field_ident = true;
        }
            },
            syn::Type::Path(type_path) if type_path.path.segments.len() == 1 && type_path.path.segments[0].ident == "Option" => {
                if let syn::PathArguments::AngleBracketed(angle_bracketed_args) = &type_path.path.segments[0].arguments {
                    if angle_bracketed_args.args.len() == 1 {
                        if let syn::GenericArgument::Type(syn::Type::Path(inner_type_path)) = &angle_bracketed_args.args[0] {
                            if inner_type_path.path.is_ident("String") {
                                quote! {
                            result.#field_ident = Some(args_iter.next().unwrap_or_default().to_string());
                        }
                            } else {
                                panic!("Unsupported type for Option: {}", quote! { #inner_type_path }.to_string());
                            }
                        } else {
                            panic!("Unsupported type for CLI argument: {}", quote! { #field.ty }.to_string());
                        }
                    } else {
                        panic!("Unsupported type for CLI argument: {}", quote! { #field.ty }.to_string());
                    }
                } else {
                    panic!("Unsupported type for CLI argument: {}", quote! { #field.ty }.to_string());
                }
            },
            other_type => {
                let type_str = quote! { #other_type }.to_string();
                panic!("Unsupported type for CLI argument: {}", type_str);
            },
        };

        let short_condition = attr.short.as_ref().map(|short| {
            let short = format!("-{}", short);
            quote! { arg == #short }
        });
        let long_condition = attr.long.as_ref().map(|long| {
            let long = format!("--{}", long);
            quote! { arg == #long }
        });

        Some(match (short_condition, long_condition) {
            (Some(sc), Some(lc)) => quote! {
            if #sc || #lc {
                #assignment_logic
            }
        },
            (Some(sc), None) => quote! {
            if #sc {
                #assignment_logic
            }
        },
            (None, Some(lc)) => quote! {
            if #lc {
                #assignment_logic
            }
        },
            (None, None) => panic!("This should not be reached."),
        })
    }).collect();


    let help_display_tokens: Vec<_> = cli_attrs.iter().filter_map(|attr| {
        if attr.short.is_none() && attr.long.is_none() {
            return None;
        }
        let short = &attr.short.as_deref().unwrap_or("");
        let long = &attr.long.as_deref().unwrap_or("");
        let desc = &attr.desc.as_deref().unwrap_or("");
        Some(quote! {
        println!("-{}, --{}    {}", #short, #long, #desc);
    })
    }).collect();

    let help_display_long_tokens: Vec<_> = cli_attrs.iter().filter_map(|attr| {
        if attr.short.is_none() && attr.long.is_none() {
            return None;
        }
        let short = &attr.short.as_deref().unwrap_or("");
        let long = &attr.long.as_deref().unwrap_or("");
        let desc = &attr.long_desc.as_deref().unwrap_or("");
        Some(quote! {
        println!("-{}, --{}    {}", #short, #long, #desc);
    })
    }).collect();

    let tokens = quote! {
        impl #struct_name {
            pub fn parse_args() -> Result<Self, CliError> {
                let mut result = Self::default();
                let mut args_iter = std::env::args().peekable();
                while let Some(arg) = args_iter.next() {
                    if arg == "-h" {
                        Self::display_help();
                        return Err(CliError::HelpRequested);
                    }

                    if arg == "--help" {
                        Self::display_long_help();
                        return Err(CliError::HelpRequested);
                    }

                    #(#arg_matching_tokens)*
                }

                // TODO: Your actual parsing logic for other args goes here

                Ok(result)
            }

            pub fn display_help() {
                println!("Usage:");
                println!("-h, --help    Displays the help message.");
                #(#help_display_tokens)*
            }

            pub fn display_long_help() {
                println!("Usage:");
                println!("-h, --help    Displays the help message.");
                #(#help_display_long_tokens)*
            }
        }
    };
    tokens.into()
}
